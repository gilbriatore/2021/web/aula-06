const express = require("express");
const path = require("path");

const port = 3000;

const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));

app.use(express.static(path.join(__dirname, "./static")));

const pratoDoDia = {
  nome: "Omelete misto",
  descricao: "Salada de ovos, filé de frango, queijo gouda, rúcula e tomate.",
  preco: "R$ 25,10",
  foto: "cafe7.jpg"
}

app.get("/", (req, res) => {
  res.render("layout/template", { conteudo:  "index", prato: pratoDoDia });
});

const dados = require("./dados/produtos.json");

app.get("/cardapio", (req, res) => {
  res.render("layout/template", { conteudo: "cardapio", produtos: dados });
});

app.get("/carrinho", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/html/carrinho.html"));
});

app.get("/sobre", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/html/sobre.html"));
});

app.get("/admin", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/html/admin/admin.html"));
});

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
}); 
